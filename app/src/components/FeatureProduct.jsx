/**
 * Created by Akash on 02/09/17.
 */
import React, { Component } from 'react';
import Slider from './slider/Slider.jsx'
import Button from './Button.jsx'
export default class FeatureProduct extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="featureProduct-Section">
                <div className="feature-body">
                    <h2>
                        FEATURE PRODUCTS
                    </h2>
                    <Slider/>
                    <Button text = {"SHOP THE RANGE"}/>
                </div>
            </div>
        );
    }
}