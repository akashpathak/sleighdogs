/**
 * Created by Akash on 02/09/17.
 */

import React, { Component } from 'react';

export default class Button extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
        <a href="#" className="rc-button">
            {this.props.text}
        </a>
    );
  }
}