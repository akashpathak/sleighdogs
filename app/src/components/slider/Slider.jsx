/**
 * Created by Akash on 02/09/17.
 */
import React, { Component } from 'react';
export default class Slider extends Component {
    constructor(props) {
        super(props);
        this.state={
            carouselData:[
                'Product Title',
                '14.95 €',
                'Lorem ipsum sit dolor amet lorem ipsum sit dolor'

            ]
        }
    }

    render() {
        return (
            <div>
                <div className="carousel" data-flickity='{ "contain": true,"cellAlign": "left","pageDots": false}'>
                    <div className="carousel-cell">
                        <div className="carousel-section">
                            <h4>
                                {this.state.carouselData[0]}
                                <span className="text-right">{this.state.carouselData[1]}</span>
                            </h4>
                            <h4>
                                {this.state.carouselData[2]}
                            </h4>
                        </div>
                    </div>
                    <div className="carousel-cell">
                        <div className="carousel-section">
                            <h4>
                                {this.state.carouselData[0]}
                                <span className="text-right">{this.state.carouselData[1]}</span>
                            </h4>
                            <h4>
                                {this.state.carouselData[2]}
                            </h4>
                        </div>
                    </div>
                    <div className="carousel-cell">
                        <div className="carousel-section">
                            <h4>
                                {this.state.carouselData[0]}
                                <span className="text-right">{this.state.carouselData[1]}</span>
                            </h4>
                            <h4>
                                {this.state.carouselData[2]}
                            </h4>
                        </div>
                    </div>
                    <div className="carousel-cell">
                        <div className="carousel-section">
                            <h4>
                                {this.state.carouselData[0]}
                            </h4>
                            <h4>
                                {this.state.carouselData[2]}
                            </h4>
                        </div>
                    </div>
                    <div className="carousel-cell">
                        <div className="carousel-section">
                            <h4>
                                {this.state.carouselData[0]}
                                <span className="text-right">{this.state.carouselData[1]}</span>
                            </h4>
                            <h4>
                                {this.state.carouselData[2]}
                            </h4>
                        </div>
                    </div>
                    <div className="carousel-cell">
                        <div className="carousel-section">
                            <h4>
                                {this.state.carouselData[0]}
                                <span className="text-right">{this.state.carouselData[1]}</span>
                            </h4>
                            <h4>
                                {this.state.carouselData[2]}
                            </h4>
                        </div>
                    </div>
                    <div className="carousel-cell">
                        <div className="carousel-section">
                            <h4>
                                {this.state.carouselData[0]}
                                <span className="text-right">{this.state.carouselData[1]}</span>
                            </h4>
                            <h4>
                                {this.state.carouselData[2]}
                            </h4>
                        </div>
                    </div>
                    <div className="carousel-cell">
                        <div className="carousel-section">
                            <h4>
                                {this.state.carouselData[0]}
                                <span className="text-right">{this.state.carouselData[1]}</span>
                            </h4>
                            <h4>
                                {this.state.carouselData[2]}
                            </h4>
                        </div>
                    </div>
                </div>
            </div>

        );
    }
}