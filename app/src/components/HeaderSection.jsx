/**
 * Created by Akash on 02/09/17.
 */
import React, { Component } from 'react';

export default class HeaderSection extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="header-Section">
                <section className="main-content">
                    <div className="header-container">
                        <h1>Lorem ipsum sit dolor lorem amet
                        </h1>
                    </div>
                </section>
            </div>
        );
    }
}