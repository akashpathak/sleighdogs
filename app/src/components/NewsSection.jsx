/**
 * Created by Akash on 02/09/17.
 */

import React, { Component } from 'react';

export default class NewsSection extends Component {
    constructor(props) {
        super(props);
        this.state={
            newsData:[
                'News title',
                'READ MORE',
                'Praesent pulvinar sapien fermentum, faucibus diam congue, ultricies turpis.',
                'Duis non ante ut odio sollicitudin dapibus phasellus non Lorem ipsum dolor sit amet consectetururna sit amet odio',
                'Lorem ipsum dolor sit amet consectetur'
            ]
        }
    }

    render() {
        return (
            <div className="featureProduct-Section newsSection-Section">
                <div className="feature-body">
                    <h2>
                        NEWS AND RESOURCES
                    </h2>
                    <div className="newsSection-grid">
                        <div className="newsSection-child">
                            <img src="https://www.dropbox.com/s/ptu1kp33adnfqea/lion.png?raw=1" alt="lionImg"/>
                            <div className="newsSectionBody">
                                <h2>
                                    {this.state.newsData[0]}
                                </h2>
                                <h2 className="">
                                    {this.state.newsData[2]}
                                </h2>
                                <h5>
                                    {this.state.newsData[1]}
                                </h5>
                            </div>
                        </div>
                        <div className="newsSection-child">
                            <img src="https://www.dropbox.com/s/ptu1kp33adnfqea/lion.png?raw=1" alt="lionImg"/>
                            <div className="newsSectionBody">
                                <h2>
                                    {this.state.newsData[0]}
                                </h2>
                                <h2 className="">
                                    {this.state.newsData[3]}
                                </h2>
                                <h5>
                                    {this.state.newsData[1]}
                                </h5>
                            </div>
                        </div>
                        <div className="newsSection-child">
                            <img src="https://www.dropbox.com/s/ptu1kp33adnfqea/lion.png?raw=1" alt="lionImg"/>
                            <div className="newsSectionBody">
                                <h2>
                                    {this.state.newsData[0]}
                                </h2>
                                <h2 className="">
                                    {this.state.newsData[4]}
                                </h2>
                                <h5>
                                    {this.state.newsData[1]}
                                </h5>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}