import React, { Component } from 'react';
import HeaderSection from './../components/HeaderSection.jsx';
import FeatureProduct from './../components/FeatureProduct.jsx';
import NewsSection from '../components/NewsSection.jsx';

export default class App extends Component {
    render() {
        return (
          <div className="app-container">
            <HeaderSection />
             <FeatureProduct />
              <NewsSection />
          </div>
        );
    }
}